ormar (0.12.2-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version.
  * Update copyright year.
  * Drop patch, no longer needed after changes upstream.
  * Add Build-Depends and test Depends for python3-asgi-lifespan, now required
    by upstream.
  * Skip failing test, reported upstream.
  * Remove pytest_cache before creating binary package.

 -- Edward Betts <edward@4angle.com>  Sat, 29 Jul 2023 07:02:31 +0100

ormar (0.12.0-3) unstable; urgency=medium

  * Patch pyproject.toml to allow SQLAlchemy newer than 1.4.42.
    (Closes: #1027063)
  * debian/tests/control: Add Depends for python3-httpx. This is a new
    dependency of python3-starlette, but it is only used for the starlette
    test client, so has not been added as a dependency of that package.

 -- Edward Betts <edward@4angle.com>  Wed, 28 Dec 2022 14:40:50 +0000

ormar (0.12.0-2) unstable; urgency=medium

  * debian/control
    - add httpx to b-d, needed by tests (via starlette.testclient);
      Closes: #1026020, #1026506
  * debian/patches/PR971.patch
    - make tests compatible with new starlette/httpx; Closes: #1026258

 -- Sandro Tosi <morph@debian.org>  Tue, 27 Dec 2022 17:43:20 -0500

ormar (0.12.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Tue, 25 Oct 2022 09:11:13 +0100

ormar (0.11.3-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 07 Sep 2022 15:01:10 +0100

ormar (0.11.2-3) unstable; urgency=medium

  * debian/upstream/metadata: fix github URLs.
  * Set PYBUILD_TEST_ARGS=--asyncio-mode=auto because pytest-asyncio 0.19
    made asyncio_mode=strict the default, which caused tests failures.
  * Build-Depends: Add <!nocheck> to packages needed for tests.

 -- Edward Betts <edward@4angle.com>  Tue, 19 Jul 2022 15:11:06 +0100

ormar (0.11.2-2) unstable; urgency=medium

  * Source-only upload to allow package to migrate to testing.

 -- Edward Betts <edward@4angle.com>  Thu, 07 Jul 2022 17:51:34 +0100

ormar (0.11.2-1) unstable; urgency=medium

  * Initial release. (Closes: #1013971)

 -- Edward Betts <edward@4angle.com>  Tue, 28 Jun 2022 10:38:52 +0100
